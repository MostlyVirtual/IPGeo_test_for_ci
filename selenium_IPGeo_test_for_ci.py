from selenium import webdriver

# Config section
api_url = 'http://ipinfo.io/json'

"""Start web driver"""
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')        
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
browser = webdriver.Chrome(chrome_options=chrome_options)
browser.implicitly_wait(10)

try:
    # Grab the desired page
    browser.get(api_url)

    # print all of the page source that was loaded
    print(browser.page_source.encode("utf-8"))

    # quit and close browser
    browser.quit()

except Exception as e:
    print(e)
