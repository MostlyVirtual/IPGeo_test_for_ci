import requests

# Config section
api_url = 'http://ipinfo.io/json'
connect_timeout = 10  # Time taken to identify the resolved ip and connect to them
read_timeout = 10  # Time taken to get response from connected server.

# Setting the headers array
request_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
    'Connection': 'close',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br'
}

try:
    req = requests.get(api_url, headers=request_headers, timeout=(connect_timeout, read_timeout))
    print(req.content)

except Exception as e:
    print(e)